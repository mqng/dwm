# Suckless dwm
## Version 6.5
### Patches
Patched using [flexipatch](https://github.com/bakkeby/dwm-flexipatch)
- actualfullscreen
- centeredmaster
- moveresize
- statuscmd with dwmblocks
- swallow
- Vanitygaps
- xrdb
